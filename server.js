/**
 * Development web-server
 */
import webpack from 'webpack';
import express from 'express';
import config from './webpack.config';
import path from 'path';
import open from 'open';

const port = 3000;
const compiler = webpack(config);

var app = express();
app.use(require('webpack-dev-middleware')(compiler, {
  hot: true,
  historyApiFallback: true,
  publicPath: config.output.publicPath,
  headers: { 'Access-Control-Allow-Origin': '*' }
}));
app.use(require('webpack-hot-middleware')(compiler));
//app.use(express.static(path.resolve(__dirname, 'dist')));

app.get('*', (req, res) => {
  res.sendFile(path.join( __dirname, './src/index.html'));
});

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    open(`http://localhost:${port}`);
  }
});
