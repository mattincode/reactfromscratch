import ReactDom from 'react-dom';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import 'babel-polyfill';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.min';
import './styles/styles.css';
import './styles/fonts/css/customfonts.css';
import './styles/typography.css';
import App from './App';

const store = configureStore();

// Example: Dispatch any actions that needs to be dispatched before any UI is loaded
//store.dispatch(loadProfitCentersWithOrganizations());

ReactDom.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('app')
);
