import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as employeeActions from '../../actions/employeeActions';

class EmployeePage extends React.Component {    
  componentWillMount() {
    this.props.employeeActions.loadEmployees();
  }

  render() {        
    const employees = this.props.employees.length === 0 ? "Loading employees..." : this.props.employees.map(emp => <li key={emp.id}>{emp.name}</li>);
    return (
    <div>
      <h2>{this.props.header}</h2>
      <ul>
         {employees}
      </ul>
    </div>
    );
  }
}

EmployeePage.propTypes = {
  header: PropTypes.string.isRequired,
  employees: PropTypes.array.isRequired,
  employeeActions: PropTypes.func
};

function mapStateToProps(state, ownProps) {
  return {
    employees: state.employees
  };
}

function mapDispatchToProps(dispatch) {
  return {
    employeeActions: bindActionCreators(employeeActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeePage);