export const KeyCode = {Tab: 9, Delete: 46, Esc: 27, Space: 32, Enter: 13, ArrowUp: 38, ArrowDown: 40, ArrowLeft: 37, ArrowRight: 39, Z: 90, F: 70};

export const SortOrder = {
  None: 0,
  Ascending: 1,
  Descending: 2
};

//TODO: refactor constructor argument order
export class GridColumn {
  constructor(key, isVisible, header, width, sortOrder, isGroupBy, filter, onRenderRowCallback, enableFilter, freeTextFilter, enableGroupBy, enableSummary) {
    this.key = key.toString();
    this.isVisible = isVisible;
    this.header = header;
    this.minWidth = 20; //TODO expose as a constant
    this.width = width;
    this.autoWidth = width;
    this.sortOrder = sortOrder ? sortOrder : SortOrder.None;
    this.enableGroupBy = enableGroupBy;
    this.groupOrder = 0;
    this.isGroupBy = (isGroupBy === undefined || isGroupBy === null) ? false : isGroupBy;
    this.isFilterEnabled = enableFilter ? enableFilter : false;
    this.filter = filter;
    this.freeTextFilter = (freeTextFilter === null || freeTextFilter === undefined) ? "" : freeTextFilter;
    this.onRenderRowCallback = onRenderRowCallback;
    this.enableSummary = enableSummary;
    //TODO: Add columnIndex if column reordering should be supported!
  }
}


