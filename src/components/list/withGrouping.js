import React from 'react';
import PropTypes from 'prop-types';
import GridGroupByHeader from './GridGroupByHeader';

/**
 * Higher order component that extends the Grid-component with functionality to group rows on columns.
 */
export function withGrouping(Grid, selectedRowKey, onGroupHeaderKeyDown, columns, groupByHeaderHeight, onColumnsUpdated) {
  class WithGrouping extends React.Component {
    constructor(props, context) {
      super(props, context);
      this.renderGroupRow = this.renderGroupRow.bind(this);
      this.onGroupAdded = this.onGroupAdded.bind(this);
      this.onGroupRemoved = this.onGroupRemoved.bind(this);
      this.onGetGroupedData = this.onGetGroupedData.bind(this);
      this.onToggleGroupCollapsed = this.onToggleGroupCollapsed.bind(this);
      this.groupIndentationWidth = 15; //px
      this.state = {collapsedGroups: []};
    }

    onGroupAdded(key, groupOrder) {
      onColumnsUpdated(columns.map(col => {
          return col.key === key ? Object.assign({}, col, {isGroupBy: true, groupOrder: groupOrder}) : col;
        })
      );
    }

    onGroupRemoved(key, groupOrder) {
      this.setState({collapsedGroups: this.state.collapsedGroups.filter(cg => cg !== key)});
      onColumnsUpdated(columns.map(col => {
        return col.key === key ? Object.assign({}, col, {isGroupBy: false, groupOrder: 0}) : col.groupOrder > groupOrder ? Object.assign({}, col, {groupOrder: col.groupOrder - 1}) : col;
        })
      );
    }

    onToggleGroupCollapsed(event) {
      let key = event.currentTarget.getAttribute('data-field');
      if (this.state.collapsedGroups.some(cg => cg === key)) {
        this.setState({collapsedGroups: this.state.collapsedGroups.filter(cg => cg !== key)});
      } else {
        this.setState({collapsedGroups: [...this.state.collapsedGroups, key]});
      }
    }

    onGetGroupedData(rows, columns) {
      let columnsWithGroupBy = columns.filter(col => col.isGroupBy);
      if (columnsWithGroupBy.length === 0) return rows;
      let orderedColumnGroups = columnsWithGroupBy.sort((a,b) => {return a.groupOrder > b.groupOrder;});
      let firstGroup = orderedColumnGroups[0];
      let childrenGroups = orderedColumnGroups.filter(cg => cg.key !== firstGroup.key);
      return this.getRowsInGroup(rows, firstGroup, childrenGroups, 'Group_');
    }

    // Recursive function to insert group rows
    getRowsInGroup(rows, groupColumn, groupColumnChildren, rowKey) {
      let rowsWithGroups = [];
      let groupedRows = rows.reduce((groups, row) => {
        let val = row[groupColumn.key];
        groups[val] = groups[val] || [];
        groups[val].push(row);
        return groups;
      }, {});
      for (let groupKey in groupedRows) {
        if (groupedRows.hasOwnProperty(groupKey)) {
          let rowsInGroup = groupedRows[groupKey];
          // Add a group row
          let groupRow = {title: groupKey, groupOrder: groupColumn.groupOrder};
          let uniqueGroupRowKey = `${rowKey}${groupColumn.key}${groupKey}`;
          let isCollapsed = this.state.collapsedGroups.some(cg => cg === uniqueGroupRowKey);
          groupRow[this.props.identityColumnKey] = uniqueGroupRowKey;
          rowsWithGroups.push(groupRow);
          // Get nested groups
          if (!isCollapsed && groupColumnChildren && groupColumnChildren.length > 0) {
            let nextGroupColumn = groupColumnChildren[0];
            let children = groupColumnChildren.filter(cg => cg.key !== nextGroupColumn.key);
            let childGroups = this.getRowsInGroup(rowsInGroup, nextGroupColumn, children, uniqueGroupRowKey);
            childGroups.forEach(row => {
              rowsWithGroups.push(row);
            });
          } else if (!isCollapsed) {
            rowsInGroup.forEach(row => {
              rowsWithGroups.push(row);
            });
          }
        }
      }
      return rowsWithGroups;
    }

    renderGroupRow(row) {
      let key = row[this.props.identityColumnKey];
      let indentation = row.groupOrder * this.groupIndentationWidth;
      let collapsed = this.state.collapsedGroups.some(cg => cg === key);
      let groupRowClass = (selectedRowKey === key) ? `grid-group-row ${this.activeRowClassName}` : "grid-group-row";
      let chevronClass = collapsed ? "glyphicon glyphicon-chevron-right" : "glyphicon glyphicon-chevron-down";
      return (
        <div className={groupRowClass} style={{height: `${this.rowHeight}px`}} data-field={key} key={key} tabIndex={-1} onKeyDown={onGroupHeaderKeyDown}>
          <div style={{position: "relative", top: "0px", left: `${indentation}px`}}>
            <span data-field={key} className={chevronClass} style={{margin: "2px"}} onClick={this.onToggleGroupCollapsed}/>
            {row.title}
          </div>
        </div>
      );
    }

    renderGroupHeader() {
      let groupColumns = columns.filter(col => col.isGroupBy);
      if (groupColumns.length === 0) return null;
      return (
        <GridGroupByHeader height={groupByHeaderHeight} groups={groupColumns} onGroupAdded={this.onGroupAdded} onGroupRemoved={this.onGroupRemoved}/>
      );
    }

    render() {
      return (
        <Grid
          rowGroupHeader={this.renderGroupHeader}
          rowGroupRenderer={this.renderGroupRow}
          onGetGroupedData={this.onGetGroupedData}
          {...this.props}/>
      );
    }
  }

  WithGrouping.propTypes = {
    identityColumnKey: PropTypes.string,
  };

  return WithGrouping;
}
