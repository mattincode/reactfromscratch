import React from 'react';
import PropTypes from 'prop-types';

class Draggable extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.cleanup = this.cleanup.bind(this);
    this.state = {drag: null};
  }

  componentWillUnmount() {
    this.cleanup();
  }

  cleanup() {
    window.removeEventListener('mouseup', this.onMouseUp);
    window.removeEventListener('mousemove', this.onMouseMove);
  }

  onMouseDown(e) {
    let drag = this.props.onDragStart(this.props.id, e);
    if (drag === null && e.button !== 0) return;
    window.addEventListener('mouseup', this.onMouseUp);
    window.addEventListener('mousemove', this.onMouseMove);
    this.setState({drag: drag});
  }

  onMouseMove(e) {
    if (this.state.drag === null) return;
    if (e.preventDefault) {
      e.preventDefault();
    }
    this.props.onDrag(this.props.id, e);
  }

  onMouseUp (e) {
    this.cleanup();
    this.props.onDragEnd(this.props.id, e, this.state.drag);
    this.setState({drag: null});
  }

  render() {
    return (
      <div className="grid-header-cell-resizeHandle" style={this.props.style} onMouseDown={this.onMouseDown}/>
    );
  }
}

/* Validate properties */
Draggable.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onDragStart: PropTypes.func,
  onDragEnd: PropTypes.func,
  onDrag: PropTypes.func,
  style: PropTypes.object    // Set style to match size of draggable handles etc.
};

Draggable.defaultProps = {
  onDragStart: () => true,
  onDragEnd: () => {},
  onDrag: () => {}
};

export default Draggable;

