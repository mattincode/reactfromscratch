import React from 'react';
import PropTypes from 'prop-types';
import ViewportCanvas from "./ViewportCanvas";
const min = Math.min;
const max = Math.max;
const floor = Math.floor;
const ceil = Math.ceil;

/***
 * Viewport is responsible for rendering large number of rows, showing a vertical scrollbar when needed (infinite scroll).
 * Only the visible rows are rendered.
 */
class Viewport extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.updateScroll = this.updateScroll.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.resetScrollStateAfterDelay = this.resetScrollStateAfterDelay.bind(this);
    this.resetScrollStateAfterDelayCallback = this.resetScrollStateAfterDelayCallback.bind(this);
    this.clearScrollTimer = this.clearScrollTimer.bind(this);
    this.getInitialGridState = this.getInitialGridState.bind(this);
    this.scrollTimerInMS = 200;
    this.resetScrollStateTimeoutId = null;
    this.state = this.getInitialGridState(this.props.rowHeight, this.props.rowCount, this.props.minHeight);
  }

  componentWillReceiveProps(nextProps, nextContext) {
    // Reset scroll if rows has been updated
    if (nextProps.toggleRowUpdated !== this.props.toggleRowUpdated) {
      this.setState(this.getInitialGridState(nextProps.rowHeight, nextProps.rowCount, nextProps.minHeight));
    }
    if (nextProps.minHeight !== this.props.minHeight) {
      this.setState({});
    }

  }

  // Initial grid state is used when properties like columns change to reset grid to top scroll position
  getInitialGridState(rowHeight, rowCount, minHeight) {
    let renderedRowsCount = ceil((minHeight - rowHeight) / rowHeight);
    let totalRowCount = min(renderedRowsCount, rowCount);
    return {
      displayStart: 0,
      displayEnd: totalRowCount,
      visibleStart: 0,
      visibleEnd: totalRowCount,
      scrollTop: 0,
      scrollLeft: 0,
      colVisibleStart: 0,
      colVisibleEnd: this.props.columnCount,
      colDisplayStart: 0,
      colDisplayEnd: this.props.columnCount,
      isScrolling: false
    };
  }

  onScroll(scrollTop, scrollLeft) {
    this.updateScroll(scrollTop, scrollLeft, this.props.minHeight, this.props.rowHeight, this.props.rowCount);
    if (this.props.onScroll) {
      this.props.onScroll(scrollTop, scrollLeft);
    }
  }

  updateScroll(scrollTop, scrollLeft, height, rowHeight, rowCount) {
    this.resetScrollStateAfterDelay();
    let renderedRowsCount = ceil(height / rowHeight);
    let visibleStart = max(0, floor(scrollTop / rowHeight));
    let visibleEnd = min(visibleStart + renderedRowsCount, rowCount);
    let displayStart = max(0, visibleStart - this.props.overScan.rowsStart);
    let displayEnd = min(visibleEnd + this.props.overScan.rowsEnd, rowCount);
    this.setState({
      displayStart: displayStart,
      displayEnd: displayEnd,
      visibleStart: visibleStart,
      visibleEnd: visibleEnd,
      isScrolling: true
    });
    //console.log(`updateScroll: renderedRows: ${renderedRowsCount}, visible: (${visibleStart}-${visibleEnd}), display: (${displayStart}-${displayEnd})`);
  }

  clearScrollTimer() {
    if (this.resetScrollStateTimeoutId) {
      clearTimeout(this.resetScrollStateTimeoutId);
    }
  }

  resetScrollStateAfterDelay() {
    this.clearScrollTimer();
    this.resetScrollStateTimeoutId = setTimeout(this.resetScrollStateAfterDelayCallback, this.scrollTimerInMS);
  }

  resetScrollStateAfterDelayCallback() {
    this.resetScrollStateTimeoutId = null;
    this.setState({isScrolling: false});
  }

  // Render a viewport with absolute positioning that hides everything outside the viewport
  // Then let the canvas use absolute positioning (which is absolute-relative to the viewport) to render rows
  render() {
    return (
      <div className="viewport" style={{padding: "0px", overflow: "hidden", position: "absolute", height: `${this.props.minHeight}px`, width: `${this.props.width}px`,  top: 0}}>
        <ViewportCanvas
          rowRenderer= {this.props.rowRenderer}
          rowCount= {this.props.rowCount}
          toggleRowUpdated = {this.props.toggleRowUpdated}
          visibleStartRowIndex={this.state.visibleStart}
          visibleEndRowIndex={this.state.visibleEnd}
          renderStartRowIndex={this.state.displayStart}
          renderEndRowIndex={this.state.displayEnd}
          height={this.props.minHeight}
          rowHeight={this.props.rowHeight}
          viewportWidth={this.props.width}
          isScrolling ={this.state.isScrolling}
          onScroll= {this.onScroll}
          scrollToRowIndex={this.props.scrollToRowIndex}
          placeholder= {this.props.placeholder}
        />
      </div>
    );
  }
}

Viewport.propTypes = {
  toggleRowUpdated: PropTypes.bool,
  width: PropTypes.number.isRequired,
  minHeight: PropTypes.number.isRequired,   // Set the minimal viewport height
  scrollToRowIndex: PropTypes.number,
  rowRenderer: PropTypes.func.isRequired,   // Responsible for rendering the content
  rowCount: PropTypes.number.isRequired,    // Total number of rows
  rowHeight: PropTypes.number.isRequired,   // Fixed value to use for calculating how many rows that fit inside the viewport
  columnCount: PropTypes.number.isRequired,
  onScroll: PropTypes.func,                 // Optional functional call for catching scroll-events
  isScrolling: PropTypes.bool,
  placeholder: PropTypes.string.isRequired,
  overScan: PropTypes.object                // Define how many rows and columns to pre-render (render but hide). Set value to customize scroll performance
};

Viewport.defaultProps = {
  overScan: {
    colsStart: 5,
    colsEnd: 5,
    rowsStart: 5,
    rowsEnd: 5
  }
};

export default Viewport;

