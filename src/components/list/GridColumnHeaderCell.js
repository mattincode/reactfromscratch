import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {SortOrder} from "./GridDefinitions";
import GridColumnResizeHandle from './GridColumnResizeHandle';

/* A column header cell with header text and filter + sort icons */
class GridColumnHeaderCell extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.renderSortSymbol = this.renderSortSymbol.bind(this);
    this.getWidthFromMouseEvent = this.getWidthFromMouseEvent.bind(this);
    this.onHeaderClick = this.onHeaderClick.bind(this);
    this.onColumnFilterClick = this.onColumnFilterClick.bind(this);
    this.onColumnResizing = this.onColumnResizing.bind(this);
    this.onColumnResizeStart = this.onColumnResizeStart.bind(this);
    this.onColumnResizeEnd = this.onColumnResizeEnd.bind(this);
    this.onDragStart = this.onDragStart.bind(this);
    this.state = {isResizing: false};
  }

  onHeaderClick(event) {
    if (this.state.isResizing) return;
    let columnKey = event.currentTarget.getAttribute('data-field');
    let column = columnKey ? this.props.columns.find(c => c.key === columnKey) : null;
    if (column) {
      let newSortOrder = (column.sortOrder + 1) > SortOrder.Descending ? SortOrder.None : column.sortOrder + 1;
      let updatedColumns = this.props.columns.map(col => {
        return Object.assign({}, col, {sortOrder: col.key === columnKey ? newSortOrder : SortOrder.None});
      });
      // Send the updated columns to the grid (the grid will update the rows and the columns collection)
      if (this.props.onChange) {
        this.props.onChange(updatedColumns);
      }
    }
  }

  onColumnResizeStart(key, e) {
    this.setState({isResizing: true});
  }

  onColumnResizing(key, e) {
    let width = this.getWidthFromMouseEvent(e);
    if (width > 0) {
      let updatedColumns = this.props.columns.map(col => {
        return col.key === key ? Object.assign({}, col, {width: width, autoWidth: width}) : col;
      });
      this.props.onResize(updatedColumns);
    }
  }

  onColumnFilterClick(event) {
    if (this.state.isResizing) return;
    this.props.onShowFilter(this.props.column);
  }

  onColumnResizeEnd(key, e) {
    this.setState({isResizing: false});
    if (this.props.onResizeEnd) {
      this.props.onResizeEnd();
    }
  }

  getWidthFromMouseEvent(e) {
    let right = e.pageX || (e.touches && e.touches[0] && e.touches[0].pageX) || (e.changedTouches && e.changedTouches[e.changedTouches.length - 1].pageX);
    let left = ReactDOM.findDOMNode(this).getBoundingClientRect().left;
    return right - left;
  }

  onDragStart(e) {
    e.persist(); // Remove from react event pooling
    e.dataTransfer.setData("text", this.props.column.key.toString());
    e.dropEffect = "move";
  }

  renderFilterSymbol(column) {
    if (!column.isFilterEnabled) return null;
    return <span style={{position: "absolute", top: "6px", right: "4px"}} onClick={this.onColumnFilterClick} className={`glyphicon glyphicon-filter grid-filter-icon ${column.filter && column.filter.length > 0 ? 'activeFilter': ''}`}/>;
  }

  renderSortSymbol(column) {
    if (column.sortOrder === SortOrder.None) return "";
    let sortOrderClass = column.sortOrder === SortOrder.Ascending ? "grid-column-sort glyphicon glyphicon-chevron-up" : "grid-column-sort glyphicon glyphicon-chevron-down";
    return <span style={{position: "absolute", top: "1px", left: `${column.autoWidth/2-1}px` }} className={sortOrderClass}/>;
  }

  render() {
    let col = this.props.column;
    let padding = 2;
    return (
      <div className="grid-column-container" key={col.key} draggable={col.enableGroupBy} onDragStart={this.onDragStart} style={{cursor: "pointer", position: "absolute", top: "0px", left: `${this.props.left}px`, height:`${this.props.height}px` , width: `${col.autoWidth}px`,  textOverflow: "ellipsis", display: "inline-block", whiteSpace: "nowrap", transform: "none"}}>
        <div className="grid-column-cell" onClick={this.onHeaderClick} style={{position: "absolute", bottom: "3px", left: `${padding}px`, width: `${col.autoWidth}px`}} data-field={col.key}>
          {col.header}
        </div>
        {this.renderFilterSymbol(col)}
        {this.renderSortSymbol(col)}
        <GridColumnResizeHandle id={col.key} onDrag={this.onColumnResizing} onDragStart={this.onColumnResizeStart} onDragEnd={this.onColumnResizeEnd}/>
      </div>
    );
  }
}


GridColumnHeaderCell.propTypes = {
  column : PropTypes.object.isRequired,
  columns: PropTypes.array.isRequired,
  height: PropTypes.number.isRequired,
  left: PropTypes.number.isRequired,
  onChange: PropTypes.func,
  onResize: PropTypes.func,
  onResizeEnd: PropTypes.func,
  onShowFilter: PropTypes.func
};

export default GridColumnHeaderCell;

