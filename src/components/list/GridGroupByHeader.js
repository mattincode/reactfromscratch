import React from 'react';
import PropTypes from 'prop-types';

// https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API
// Issues: http://mereskin.github.io/dnd/
class GridGroupByHeader extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onDragOver = this.onDragOver.bind(this);
    this.onDragEnter = this.onDragEnter.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.onRemoveClicked = this.onRemoveClicked.bind(this);
    this.renderGroups = this.renderGroups.bind(this);
    this.groupCount = 0;
  }

  onDragOver(event) {
    event.preventDefault();
   // event.dataTransfer.dropEffect = "move"; // Does not work in IE11
    // IE11 workaround : This does only exist to get IE11 drag & drop working
    // see: https://developer.mozilla.org/en-US/docs/Web/Events/drop
    // and: https://stackoverflow.com/questions/39710421/ie11-drag-drop-not-working
  }

  onDrop(event) {
    let key = event.dataTransfer.getData("text");
    if (!this.props.groups.find(g => g.key === key)) {
      this.groupCount += 1;
      this.props.onGroupAdded(key, this.groupCount);
    }
  }

  onDragEnter(event) {
    event.preventDefault();
    // IE11 workaround : This does only exist to get IE11 drag & drop working
    // see: https://developer.mozilla.org/en-US/docs/Web/Events/drop
    // and: https://stackoverflow.com/questions/39710421/ie11-drag-drop-not-working
  }

  onRemoveClicked(event) {
    let columnKey = event.currentTarget.getAttribute('data-field');
    this.groupCount -= 1;
    this.props.onGroupRemoved(columnKey, this.groupCount);
  }

  //TODO: Hide group (do not render rows in collapsed group)
  renderGroups(groups) {
    let groupsInOrder = groups.sort((a,b) => {return a.groupOrder > b.groupOrder;});
    return (
      <div>
        <span style={{marginRight: "2px"}}>Grouped by:</span>
        {groupsInOrder.map(column => {
          return (
            <div style={{display: "inline-block"}} className="grid-header-row grid-group-by-cell" key={column.key}>{column.header}
              <i data-field={column.key} className="glyphicon glyphicon-remove" onClick={this.onRemoveClicked}/>
            </div>
          );
        })}
      </div>
    );
  }
//            <div style={{display: "inline-block"}} className="grid-header-row grid-group-by-cell" key={column.key}>{column.header}<i style={{paddingRight: "2px", paddingLeft: "4px", fontSize: "6px", color: "white"}} className="close"><i className="glyphicon glyphicon-remove"/></i></div>
  render() {
    let hasGroups = this.props.groups && this.props.groups.length > 0;
    let groupByLabel = "Drag a column here to group";
    return (
      <div className="grid-group-by-drop" onDragOver={this.onDragOver} onDragEnter={this.onDragEnter} onDrop={this.onDrop} style={{height: `${this.props.height}px`}}>
        {!hasGroups && <span>{groupByLabel}</span>}
        {hasGroups && this.renderGroups(this.props.groups)}
      </div>
    );
  }
}

/* Validate properties */
GridGroupByHeader.propTypes = {
  height: PropTypes.number.isRequired,
  groups: PropTypes.array.isRequired,
  onGroupAdded: PropTypes.func,
  onGroupRemoved: PropTypes.func
};

/* Export the class and connect it to the redux store */
export default GridGroupByHeader;

