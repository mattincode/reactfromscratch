import React from 'react';
import PropTypes from 'prop-types';
import Draggable from './Draggable';

/* Displays a resize handle absolute positioned on a grid column */
const GridColumnResizeHandle = ({id, onDragStart, onDragEnd, onDrag}) => {
  const style = {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 6,
    height: '100%'
  };
  return (
    <Draggable id={id} style={style} onDragStart={onDragStart} onDrag={onDrag} onDragEnd={onDragEnd}/>
  );
};

GridColumnResizeHandle.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onDragStart: PropTypes.func,
  onDragEnd: PropTypes.func,
  onDrag: PropTypes.func,
};

export default GridColumnResizeHandle;
