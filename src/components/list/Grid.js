import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Viewport from './Viewport';
import GridHeader from './GridHeader';
import GridColumnHeader from './GridColumnHeader';
import GridGroupByHeader from './GridGroupByHeader';
import GridSummaryRow from './GridSummaryRow';
import GridXlsxExport from './GridXlsxExport';
import {SortOrder, KeyCode, GridColumn} from "./GridDefinitions";
import {isValueArrayEqual} from "../../utils/shallowEqual";
import './Grid.css';
import {keyCode} from "../../models/constants/keyCodes";

/* Grid component that display a list of data */
class Grid extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.renderRow = this.renderRow.bind(this);
    this.renderGroupRow = this.renderGroupRow.bind(this);
    this.updateMetrics = this.updateMetrics.bind(this);
    this.updateColumnMetrics = this.updateColumnMetrics.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.onRowClick = this.onRowClick.bind(this);
    this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
    this.onRowContextMenu = this.onRowContextMenu.bind(this);
    this.onRowKeyDown = this.onRowKeyDown.bind(this);
    this.onColumnsChanged = this.onColumnsChanged.bind(this);
    this.onGetColumnFilterData = this.onGetColumnFilterData.bind(this);
    this.getSortedAndFilteredData = this.getSortedAndFilteredData.bind(this);
    this.getGroupedData = this.getGroupedData.bind(this);
    this.getRowsInGroup = this.getRowsInGroup.bind(this);
    this.getFilteredData = this.getFilteredData.bind(this);
    this.setFocusToActiveRow = this.setFocusToActiveRow.bind(this);
    this.setAutoColumnWidths = this.setAutoColumnWidths.bind(this);
    this.updateRows = this.updateRows.bind(this);
    this.getTextDisplaySize = this.getTextDisplaySize.bind(this);
    this.onGroupAdded = this.onGroupAdded.bind(this);
    this.onGroupRemoved = this.onGroupRemoved.bind(this);
    this.onToggleGroupCollapsed = this.onToggleGroupCollapsed.bind(this);
    this.isRowUpdateRequired = this.isRowUpdateRequired.bind(this);
    this.activeRowClassName = 'active';
    this.textLengthElement = null;
    this.visibleColumns = [];
    this.sortedAndFilteredRows = [];
    this.rowHeight = this.props.rowHeight;
    this.settingsColumnWidth = 15;
    this.groupIndentationWidth = 15;
    this.state = {
      collapsedGroups: [],
      selectedRowKey: this.props.selectedRowKey ? this.props.selectedRowKey.toString() : "",
      scroll: {scrollTop: 0, scrollLeft: 0},
      containerWidth: 0,
      containerHeight: 0,
      rowsUpdatedToggle: false,
      columns: this.props.columns.map(col => col.key === this.props.identityColumnKey ? Object.assign({}, col,{isVisible: false, isGroupBy: false}) : col), // Always hide the identity column
      selectAllColumns: false,
    };
  }

  componentWillMount() {
    this.updateRows(this.props.rows, this.state.columns, this.state.collapsedGroups);
  }

  componentDidMount() {
    this.textLengthElement = document.getElementById("textLength");
    this.setAutoColumnWidths(this.sortedAndFilteredRows, this.visibleColumns);
    this.updateMetrics();
    this.setFocusToActiveRow();
    window.addEventListener('resize', this.updateMetrics);
  }

  componentWillReceiveProps(nextProps, nextContext) {
    let nextSelectedRow = nextProps.selectedRowKey ? nextProps.selectedRowKey.toString() : "";
    if (nextSelectedRow !== this.props.selectedRowKey) {
      this.setState({selectedRowKey: nextSelectedRow});
    }
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    if (this.state.collapsedGroups.length !== nextState.collapsedGroups.length || this.isRowUpdateRequired(this.state.columns, nextState.columns)) {
      this.updateRows(nextProps.rows, nextState.columns, nextState.collapsedGroups);
    }
  }

  componentDidUpdate() {
    this.setFocusToActiveRow();
  }

  isRowUpdateRequired(currentColumns, nextColumns) {
    return nextColumns.some(nextColumn => {
      let currentColumn = currentColumns.find(c => c.key === nextColumn.key);
      if (!currentColumn) return true;                                                  // Update if the column collection has changed
      if (nextColumn.freeTextFilter !== currentColumn.freeTextFilter) return true;      // Update if filter changed
      if (!isValueArrayEqual(nextColumn.filter, currentColumn.filter)) return true;   // Update if filter changed
      if (nextColumn.isGroupBy !== currentColumn.isGroupBy) return true;                // Update if group by changed
      if (nextColumn.sortOrder !== currentColumn.sortOrder) return true;
    });
  }

  updateRows(rows, columns, collapsedGroups) {
    // Update a local variable since we do not want to affect the state
    this.sortedAndFilteredRows = this.getSortedAndFilteredData(rows, columns, collapsedGroups);
  }

  /******************************* Events for columns and scroll ************************************/

  onScroll(scrollTop, scrollLeft) {
    this.setState({scroll: {scrollTop: scrollTop, scrollLeft: scrollLeft}});
  }

  onColumnsChanged(updatedColumns) {
    let shouldUpdateRows = this.isRowUpdateRequired(this.state.columns, updatedColumns);
    this.setState({
      columns: updatedColumns,
      selectedRowKey: shouldUpdateRows ? "" : this.state.selectedRowKey,
      rowsUpdatedToggle: !this.state.rowsUpdatedToggle
    });
  }
  /*************************************************************************************/

  /******************************* Column and container metrics (width and height) ************************************/
  updateMetrics() {
    let parentNode = ReactDOM.findDOMNode(this).parentElement;
    let parentRect = parentNode.getBoundingClientRect();
    let height = Math.min(this.props.maxHeight, window.innerHeight - parentRect.top - 10); // TODO: Uglyfuck temporary solution, that will set height related to the browser window assuming that there is nothing below
    this.setState({containerWidth: parentNode.offsetWidth, containerHeight: height, rowsUpdatedToggle: !this.state.rowsUpdatedToggle});
  }

  // Adjust column width to fit the largest text in the list (this should only be done initially)
  // Naive impl. since different characters has different width
  setAutoColumnWidths(rows, columns) {
    let updatedColumns = [];
    columns.forEach(col => {
      if (col.width) return; // Do not auto set width if value already provided
      let longestTextLength = 0;
      let longestText = "";
      rows.forEach(r => {
        let text = r[col.key].toString();
        if (text.length > longestTextLength) {
          longestTextLength = text.length;
          longestText = text;
        }
      });
      let columnDisplaySize = this.getTextDisplaySize(longestText);
      if (columnDisplaySize !== col.autoWidth) {
        col.autoWidth = columnDisplaySize + 10;
        updatedColumns.push(col);
      }
    });
    if (updatedColumns.length > 0) {
      this.setState({columns: this.state.columns.map(col => {
        let updated = updatedColumns.find(c => c.key === col.key);
          return updated ? updated : col;
        })
      });
    }
  }

  getTextDisplaySize(text) {
    if (!this.textLengthElement) return null;
    this.textLengthElement.innerHTML = text;
    return this.textLengthElement.offsetWidth;
  }

  updateColumnMetrics(groupCount) {
    if (!this.visibleColumns) return;
    let colPos = this.settingsColumnWidth + this.groupIndentationWidth * groupCount;
    this.visibleColumns.forEach(col => {
      col.autoWidth = col.width ? col.width : col.autoWidth ? col.autoWidth : col.minWidth ;
      col.left = colPos;
      colPos = colPos + col.autoWidth;
    });
  }

  /*************************************************************************************/

  /******************************* Row selection ************************************/

  onRowClick(event) {
    let rowKey = event.currentTarget.getAttribute('data-field');
    //console.log("Row click" + rowKey);
    this.setState({selectedRowKey: rowKey});
    if (this.props.onRowClick) {
      this.props.onRowClick(rowKey);
    }
  }

  onRowDoubleClick(event) {
    let rowKey = event.currentTarget.getAttribute('data-field');
    //console.log("Double click" + rowKey);
    if (this.props.onRowDoubleClick) {
      this.props.onRowDoubleClick(rowKey);
    }
  }

  onRowContextMenu(event) {
    event.preventDefault();
    let rowKey = event.currentTarget.getAttribute('data-field');
    this.setState({selectedRowKey: rowKey});
    //console.log("Context menu click" + rowKey);
    if (this.props.onRowContextMenu) {
      this.props.onRowContextMenu(event, rowKey);
    }
  }

  onRowKeyDown(event) {
    event.preventDefault();  // Prevent canvas from scrolling when pressing keys
    let rowKey = event.currentTarget.getAttribute('data-field');
    if (rowKey.startsWith("Group_") && (event.keyCode === KeyCode.ArrowRight || event.keyCode === KeyCode.ArrowLeft)) {
      // Toggle group expanded
      if (this.state.collapsedGroups.some(cg => cg === rowKey)) {
        this.setState({collapsedGroups: this.state.collapsedGroups.filter(cg => cg !== rowKey)});
      } else {
        this.setState({collapsedGroups: [...this.state.collapsedGroups, rowKey]});
      }
    } else { // Handle row keyboard navigation
      // Note that we can navigate a that is outside the viewport visible area since we pre/post render rows using overscan.
      let rowKeyIndex = this.sortedAndFilteredRows.findIndex(row => row[this.props.identityColumnKey].toString() === rowKey);
      if (event.keyCode === KeyCode.ArrowUp && rowKeyIndex > 0) {
        this.setState({selectedRowKey: this.sortedAndFilteredRows[rowKeyIndex-1][this.props.identityColumnKey].toString()});
      } else if (event.keyCode === KeyCode.ArrowDown && rowKeyIndex < this.sortedAndFilteredRows.length-1) {
        this.setState({selectedRowKey: this.sortedAndFilteredRows[rowKeyIndex+1][this.props.identityColumnKey].toString()});
      }
      // Send row click on keyboard navigation
      if (this.props.onRowClick) {
        this.props.onRowClick(rowKey);
      }
    }
  }

  setFocusToActiveRow() {
    if (this.state.isScrolling) return;
    let activeRow = document.getElementsByClassName(this.activeRowClassName);
    if (activeRow && activeRow[0]) {
      activeRow[0].focus();
    }
  }
  /*************************************************************************************/

  /******************************* Column filter ************************************/

  onGetColumnFilterData(column) {
    let filterValues = [];
    this.props.rows.forEach(item => {
      let columnValue = item[column.key];
      if (!filterValues.some(val => val === columnValue)) {
        filterValues.push(columnValue);
      }
    });
    return filterValues;
  }

  getFilteredData(rows, filteredColumns) {
    if (!filteredColumns || filteredColumns.length === 0) return rows;
    let filteredRows = [];
    rows.forEach(row => {
      let isIncluded = filteredColumns.every(column => {
        let columnText = row[column.key].toString();
        if (column.freeTextFilter && column.freeTextFilter.length > 0) {
          return columnText.includes(column.freeTextFilter);
        } else {
          return column.filter.some(f => f === columnText);
        }
      });
      if (isIncluded) {
        filteredRows.push(row);
      }
    });
    return filteredRows;
  }

  getSortedAndFilteredData(rows, columns, collapsedGroups) {
    let groupedColumns = columns.filter(c => c.isGroupBy);
    let filteredColumns = columns.filter(col => (col.filter && col.filter.length > 0) || (col.freeTextFilter && col.freeTextFilter.length > 0));
    let filteredRows = filteredColumns.length === 0 ? rows : this.getFilteredData(rows, filteredColumns);
    const sortColumn = columns.find(col => col.sortOrder !== SortOrder.None);
    let sortedAndFilteredRows = (!sortColumn || filteredRows.length === 0) ? filteredRows : filteredRows.sort((a,b) => {
      if (sortColumn.sortOrder === SortOrder.Ascending) {
        return a[sortColumn.key] >= b[sortColumn.key] ? 1 : -1;
      } else {
        return a[sortColumn.key] <= b[sortColumn.key] ? 1 : -1;
      }
    });
    if (groupedColumns.length > 0) {
      return this.getGroupedData(sortedAndFilteredRows, groupedColumns, collapsedGroups);
    } else {
      return sortedAndFilteredRows;
    }
  }

  /******************************* Grouping ************************************/
  onGroupAdded(key, groupOrder) {
    this.setState({columns: this.state.columns.map(col => {
        return col.key === key ? Object.assign({}, col, {isGroupBy: true, groupOrder: groupOrder}) : col;
      })
    });
  }

  onGroupRemoved(key, groupOrder) {
    this.setState({
      collapsedGroups: this.state.collapsedGroups.filter(cg => cg !== key),
      columns: this.state.columns.map(col => {
        return col.key === key ? Object.assign({}, col, {isGroupBy: false, groupOrder: 0}) : col.groupOrder > groupOrder ? Object.assign({}, col, {groupOrder: col.groupOrder - 1}) : col;
      })
    });
  }

  onToggleGroupCollapsed(event) {
    let key = event.currentTarget.getAttribute('data-field');
    if (this.state.collapsedGroups.some(cg => cg === key)) {
      this.setState({collapsedGroups: this.state.collapsedGroups.filter(cg => cg !== key)});
    } else {
      this.setState({collapsedGroups: [...this.state.collapsedGroups, key]});
    }
  }

  // Add group-rows (including sub-groups)
  // Make sure that the data is sorted based on the groups first and foremost
  getGroupedData(rows, columnsWithGroupBy, collapsedGroups) {
    if (columnsWithGroupBy.length === 0) return rows;
    let orderedColumnGroups = columnsWithGroupBy.sort((a,b) => {return a.groupOrder > b.groupOrder;});
    let firstGroup = orderedColumnGroups[0];
    let childrenGroups = orderedColumnGroups.filter(cg => cg.key !== firstGroup.key);
    return this.getRowsInGroup(rows, firstGroup, childrenGroups, 'Group_', collapsedGroups);
  }

  // Recursive to get all rows in a group including subgroups (children)
  // groupColumnChildren is ordered in groupOrder
  getRowsInGroup(rows, groupColumn, groupColumnChildren, rowKey, collapsedGroups) {
    let rowsWithGroups = [];
    let groupedRows = rows.reduce((groups, row) => {
      let val = row[groupColumn.key];
      groups[val] = groups[val] || [];
      groups[val].push(row);
      return groups;
    }, {});
    for (let groupKey in groupedRows) {
      let rowsInGroup = groupedRows[groupKey];
      // Add a group row
      let groupRow = {title: groupKey, groupOrder: groupColumn.groupOrder};
      let uniqueGroupRowKey = `${rowKey}${groupColumn.key}${groupKey}`;
      let isCollapsed = collapsedGroups.some(cg => cg === uniqueGroupRowKey);
      groupRow[this.props.identityColumnKey] = uniqueGroupRowKey;
      rowsWithGroups.push(groupRow);
      // Get nested groups
      if (!isCollapsed && groupColumnChildren && groupColumnChildren.length > 0) {
        let nextGroupColumn = groupColumnChildren[0];
        let children = groupColumnChildren.filter(cg => cg.key !== nextGroupColumn.key);
        let childGroups = this.getRowsInGroup(rowsInGroup, nextGroupColumn, children, uniqueGroupRowKey, collapsedGroups);
        childGroups.forEach(row => {
          rowsWithGroups.push(row);
        });
       } else if (!isCollapsed) {
        rowsInGroup.forEach(row => {
          rowsWithGroups.push(row);
        });
      }
    }
    return rowsWithGroups;
  }

  /*************************************************************************************/
  /*********************************** Rendering ***************************************/

  renderGroupRow(row) {
    let key = row[this.props.identityColumnKey].toString();
    let indentation = row.groupOrder * this.groupIndentationWidth;
    let collapsed = this.state.collapsedGroups.some(cg => cg === key);
    let groupRowClass = (this.state.selectedRowKey === key) ? `grid-group-row ${this.activeRowClassName}` : "grid-group-row"; // == is intentional to compare both string and number keys
    let chevronClass = collapsed ? "glyphicon glyphicon-chevron-right" : "glyphicon glyphicon-chevron-down";
    return (
      <div className={groupRowClass} style={{height: `${this.rowHeight}px`}} data-field={key} key={key} tabIndex={-1} onKeyDown={this.onRowKeyDown}>
        <div style={{position: "relative", top: "0px", left: `${indentation}px`}}>
          <span data-field={key} className={chevronClass} style={{margin: "2px"}} onClick={this.onToggleGroupCollapsed}/>
          {row.title}
        </div>
      </div>
    );
  }


  renderRow(index) {
    let row = this.sortedAndFilteredRows[index];
    if (!row) return;
    let rowKey = row[this.props.identityColumnKey].toString();
    if (rowKey.startsWith("Group_"))
      return this.renderGroupRow(row);

    let selectedClass = (this.state.selectedRowKey === rowKey) ? this.activeRowClassName : "";
    return (
      <div data-field={rowKey} className={selectedClass} style={{height: `${this.rowHeight}px`}} key={rowKey} onClick={this.onRowClick} onDoubleClick={this.onRowDoubleClick} tabIndex={-1} onKeyDown={this.onRowKeyDown} onContextMenu={this.onRowContextMenu}>
        {this.visibleColumns.map(col => {
          return (
            <div key={col.key} style={{position: "absolute", overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis", left: `${col.left}px`, width: `${col.width ? col.width : col.autoWidth}px`}}>
              {col.onRenderRowCallback ? col.onRenderRowCallback(row, row[col.key]) : row[col.key]}
            </div>
          );
        })}
      </div>
    );
  }

  render() {
    let hasSummaryRow = this.props.onSummaryRowCallback;
    let groupByEnabled = this.state.columns.some(c => c.enableGroupBy);
    let groupColumns = this.state.columns.filter(c => c.isGroupBy);
    this.visibleColumns = this.state.columns.filter(c => c.isVisible);
    let selectedRowIndex = this.state.selectedRowKey ? this.sortedAndFilteredRows.findIndex(row => row[this.props.identityColumnKey].toString() === this.state.selectedRowKey) : 0;
    let totalRowCount = this.props.rows.length;
    this.updateColumnMetrics(groupColumns.length); //TODO: Move... this should only be updated on change
    let padding = 2;
    let viewportWidth = this.state.containerWidth - padding;
    let headerHeight = 0;  //TODO: This should be calculated based on text size
    let columnHeaderHeight = 0;
    let listHeaderHeight = 0;
    let groupByHeaderHeight = 0;
    let summaryRowHeight = hasSummaryRow ? this.rowHeight : 0;
    if (groupByEnabled) {
      headerHeight = 60;
      columnHeaderHeight = headerHeight /3;
      listHeaderHeight = headerHeight /3;
      groupByHeaderHeight = headerHeight /3;
    } else {
      headerHeight = 40;
      columnHeaderHeight = headerHeight /2;
      listHeaderHeight = headerHeight /2;
    }
    let groupsIndentation = groupColumns.length * this.groupIndentationWidth;
    let viewportHeight =  this.state.containerHeight - headerHeight - summaryRowHeight;
    let buttons = <GridXlsxExport rows={this.sortedAndFilteredRows} columns={this.visibleColumns} />; //TODO: Add buttons sent in this.props.buttons
    return (
      <div className="grid-container" style={{width: `${viewportWidth}px`}}>
        <div className="grid-main" style={{clear: "both"}}>
          <GridHeader columns={this.visibleColumns} rowCount={this.sortedAndFilteredRows.length} totalRowCount={totalRowCount} height={listHeaderHeight} buttons={buttons}/>
          {groupByEnabled && <GridGroupByHeader height={groupByHeaderHeight} groups={groupColumns} onGroupAdded={this.onGroupAdded} onGroupRemoved={this.onGroupRemoved}/>}
          <GridColumnHeader
            height={columnHeaderHeight}
            width={viewportWidth}
            identityColumnKey={this.props.identityColumnKey}
            scrollLeft={this.state.scroll.scrollLeft}
            columns={this.state.columns}
            settingsColumnWidth={this.settingsColumnWidth}
            groupColumnsWidth = {groupsIndentation}
            onChange={this.onColumnsChanged}
            onGetColumnFilterData={this.onGetColumnFilterData} />
          <div className="grid-grid" style={{position: "relative", padding: `${padding}px`,  minHeight: `${viewportHeight}px`}}>
            <Viewport
              placeholder={this.props.placeholder}
              width={viewportWidth}
              rowHeight={this.rowHeight}
              scrollToRowIndex={selectedRowIndex}
              rowRenderer={this.renderRow}
              toggleRowUpdated={this.state.rowsUpdatedToggle}
              minHeight={viewportHeight}
              columnCount={this.visibleColumns.length}
              rowCount={this.sortedAndFilteredRows.length}
              onScroll={this.onScroll}
            />
          </div>
          {hasSummaryRow &&
            <GridSummaryRow
              identityColumnKey={this.props.identityColumnKey}
              onSummaryRowCallback={this.props.onSummaryRowCallback}
              height={summaryRowHeight}
              width={viewportWidth}
              settingsColumnWidth={this.settingsColumnWidth}
              groupColumnsWidth={groupsIndentation}
              scrollLeft={this.state.scroll.scrollLeft}
              rows = {this.sortedAndFilteredRows}
              columns={this.state.columns}/>
          }
        </div>
        <span id="textLength" style={{position: "absolute", top: 0, left: 0, zIndex: -100, color: "#FFF"}}/>
      </div>
    );
  }
}

/* Validate properties */
Grid.propTypes = {
  identityColumnKey: PropTypes.string.isRequired,   // Column key that should represent the identity(key) in a row
  rows: PropTypes.array.isRequired,                 // Array of data rows
  columns: PropTypes.array.isRequired,              // Array of GridColumn (from GridDefinitions.js)
  placeholder: PropTypes.string,                    // Empty list placeholder text
  selectedRowKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]), // Set value of row to select and set focus on (row[identityColumnKey])
  onRowClick: PropTypes.func,
  onRowDoubleClick: PropTypes.func,
  onRowContextMenu: PropTypes.func,                 // (event, rowKey) Context menu click on a row
  onSummaryRowCallback: PropTypes.func,             // (colKey, rows) Callback with column-key + all visible rows
  maxHeight: PropTypes.number,                      // Grid max height (total grid with headers and scrollbars), set this to limit the max height.
  rowHeight: PropTypes.number,                      // Row fixed height (in the future we may set this dyanmically based on content + handle multiline-rows)
  buttons: PropTypes.element                        // Custom buttons (or other content) to display to the left in the GridHeader
};

Grid.defaultProps = {
  placeholder: "No data",
  selectedRowKey: null,
  maxHeight: 8000,
  rowHeight: 15
};

export default Grid;

