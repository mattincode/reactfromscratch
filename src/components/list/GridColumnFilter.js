import React from 'react';
import PropTypes from 'prop-types';

/* Component for displaying a dropdown with a list of the distinct values in a column that the user can select to filter the content of the grid */
class GridColumnFilter extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onClearFilterClicked = this.onClearFilterClicked.bind(this);
    this.onFilterClicked = this.onFilterClicked.bind(this);
    this.onFreeTextChanged = this.onFreeTextChanged.bind(this);
    this.renderFilters = this.renderFilters.bind(this);
    this.onSaveColumnFilter = this.onSaveColumnFilter.bind(this);
    this.state = {
      filterData: this.props.onGetColumnFilterData(this.props.column),
      freeTextFilter: this.props.column.freeTextFilter ? this.props.column.freeTextFilter : '',
      checked: this.props.column.filter ? this.props.column.filter : []};
  }

  onFreeTextChanged(event) {
    this.setState({freeTextFilter: event.target.value, checked: []});
  }

  onClearFilterClicked(){
    this.setState({checked: [], freeTextFilter: ''});
  }

  onFilterClicked(event) {
    let filter = event.currentTarget.getAttribute('data-field');
    if (this.state.checked.find(f => f === filter)) {
      this.setState({checked: this.state.checked.filter(f => f !== filter)});
    } else {
      this.setState({checked: [...this.state.checked, filter], freeTextFilter: ''});
    }
  }

  renderFilters(){
    let checkedFilters = this.state.checked;
    return this.state.filterData.map(filter => {
      let isChecked = checkedFilters.length === 0 ? false : checkedFilters.some(f => f === filter.toString());
      return (
        <div key={filter} className="checkbox">
          <label htmlFor={filter}>
            <input data-field={filter} type="checkbox" onChange={this.onFilterClicked} checked={isChecked}/>
            {filter}
          </label>
        </div>
      );
    });
  }

  onSaveColumnFilter(event) {
    let updatedColumn = Object.assign({}, this.props.column, {filter: this.state.checked, freeTextFilter: this.state.freeTextFilter});
    this.props.onSaveFilter(updatedColumn);
  }

  render() {
    let clearFilterLabel = "Clear Filter";
    let filterListLabel = "Choices"; //TODO Translate this.context.t("TableGrid_Filter_Choices",{},"Choices")
    let freeTextFilterLabel = "Free text"; //TODO Translate this.context.t("TableGrid_Filter_FreeText",{},"Free Text")
    return (
      <div className="dropdown open" style={{position: "relative", top:"0px", left: `${-this.props.column.left}px`, zIndex:"10000"}}>
        <div className={`filter-rounded-box`}>
          <span className="filter-choices-clear" onClick={this.onClearFilterClicked}>{clearFilterLabel}</span>
          <div>
            <div className="field">
              <label htmlFor="choices">{filterListLabel}:</label>
              <div className="filter-choices-box">
                {this.renderFilters()}
              </div>
            </div>
            <div className="field">
              <label htmlFor="text">{freeTextFilterLabel}:</label>
              <input id="text" type="text" className="form-control" onChange={this.onFreeTextChanged} value={this.state.freeTextFilter}/>
            </div>
          </div>
          <button type="button" onClick={this.onSaveColumnFilter}>Save settings</button>
          <button type="button" onClick={this.props.onClose} style={{marginLeft: "10px"}}>Close</button>
        </div>
      </div>
    );
  }
}

/* Validate properties */
GridColumnFilter.propTypes = {
  column: PropTypes.object.isRequired,
  onGetColumnFilterData: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onSaveFilter: PropTypes.func.isRequired
};

export default GridColumnFilter;
