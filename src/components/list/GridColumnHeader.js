import React from 'react';
import PropTypes from 'prop-types';
import GridColumnHeaderCell from './GridColumnHeaderCell';
import GridColumnFilter from './GridColumnFilter';
import GridSettings from './GridSettings';
import {isArrayEqual} from "../../utils/shallowEqual";

/* Component for displaying column headers for all columns in a grid. */
/* Contains a settings-button to hide/show certain columns*/
class GridColumnHeader extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.renderColumns = this.renderColumns.bind(this);
    this.renderFilterDropDown = this.renderFilterDropDown.bind(this);
    this.onClickSettings = this.onClickSettings.bind(this);
    this.onSaveColumnSettings = this.onSaveColumnSettings.bind(this);
    this.onCloseColumnSettings = this.onCloseColumnSettings.bind(this);
    this.onCloseFilter = this.onCloseFilter.bind(this);
    this.onResize = this.onResize.bind(this);
    this.onResizeEnd = this.onResizeEnd.bind(this);
    this.onSaveFilter = this.onSaveFilter.bind(this);
    this.onShowColumnFilter = this.onShowColumnFilter.bind(this);
    this.state = {
      activeFilterColumn: null,
      showColumnSettingsDropdown: false,
      columns: this.props.columns
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (!isArrayEqual(nextProps.columns, this.state.columns, this.props.identityColumnKey)) {
      this.setState({columns: nextProps.columns});
    }
  }

  onClickSettings(event) {
    this.setState({showColumnSettingsDropdown: true, columns: this.props.columns});
  }

  renderFilterDropDown() {
    if (!this.props.onGetColumnFilterData) return;
    return <GridColumnFilter column={this.state.activeFilterColumn} onClose={this.onCloseFilter} onSaveFilter={this.onSaveFilter} onGetColumnFilterData={this.props.onGetColumnFilterData}/>;
  }

  onSaveColumnSettings(columns) {
    if (this.props.onChange) {
      this.props.onChange(columns);
    }
    this.setState({showColumnSettingsDropdown: false});
  }

  onSaveFilter(column) {
    this.setState({activeFilterColumn: null});
    if (this.props.onChange) {
      let updatedColumns = this.props.columns.map(col => {
        return col.key === column.key ? column : col;
      });
      this.props.onChange(updatedColumns);
    }
  }

  onResize(updatedColumns) {
    this.setState({columns: updatedColumns});
  }

  onResizeEnd() {
    this.props.onChange(this.state.columns);
  }

  onShowColumnFilter(column) {
    this.setState({activeFilterColumn: column});
  }

  onCloseFilter() {
    this.setState({activeFilterColumn: null});
  }

  onCloseColumnSettings(event) {
    this.setState({showColumnSettingsDropdown: false});
  }

  renderColumns() {
    let colPos = this.props.settingsColumnWidth+this.props.groupColumnsWidth-this.props.scrollLeft;
    let nextPos = colPos;
    let columns = [];
    // Add settings column
    columns.push(<div key="settings" onClick={this.onClickSettings} style={{cursor: "pointer", position: "absolute", left: `${-this.props.scrollLeft}px`, top: "5px", width:`${this.props.settingsColumnWidth}px`, paddingLeft: "2px", paddingRight: "2px"}}><span className="glyphicon glyphicon-cog"/></div>);
    // Add all visible columns
    this.state.columns.forEach(col => {
      if (!col.isVisible) return;
      colPos = nextPos;
      nextPos = colPos + col.autoWidth;
      columns.push(
        <GridColumnHeaderCell key={col.key} left={colPos} height={this.props.height} onChange={this.props.onChange} onResize={this.onResize} onResizeEnd={this.onResizeEnd} onShowFilter={this.onShowColumnFilter} column={col} columns={this.state.columns}/>
      );
    });
    return columns;
  }

  render() {
    return (
      <div style={{position: "relative"}}>
        <div className="grid-header" style={{position: "relative", height: `${this.props.height}px`}}>
          <div className="grid-header-row" style={{position: "absolute", top: "0px", left: "0px", overflowX: "hidden", width: `${this.props.width}px`, minHeight: "auto"}}>
            <div style={{width: "100%", height: `${this.props.height}px`, overflow: "hidden", whiteSpace: "nowrap"}}>
              {this.renderColumns()}
            </div>
          </div>
        </div>
        {this.state.showColumnSettingsDropdown && <GridSettings identityColumnKey={this.props.identityColumnKey} columns={this.state.columns} onSaveSettings={this.onSaveColumnSettings} onClose={this.onCloseColumnSettings}/>}
        {this.state.activeFilterColumn && this.renderFilterDropDown()}
      </div>
    );
  }
}

/* Validate properties */
GridColumnHeader.propTypes = {
  identityColumnKey: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  groupColumnsWidth: PropTypes.number,
  columns: PropTypes.array.isRequired,
  scrollLeft: PropTypes.number.isRequired,
  onChange: PropTypes.func,
  onGetColumnFilterData: PropTypes.func,
  settingsColumnWidth: PropTypes.number.isRequired
};

/* Export the class and connect it to the redux store */
export default GridColumnHeader;
