import React from 'react';
import PropTypes from 'prop-types';

class GridHeader extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    let searchResultText = "Search result"; //TODO: Translate
    return (
      <div>
        <div className="grid-list-header" style={{display: "inline-block", height: `${this.props.height}px`}}>
          {`${searchResultText} ${this.props.rowCount > 0 ? `${this.props.rowCount}(${this.props.totalRowCount})` : ""}`}
        </div>
        <span className="pull-right">{this.props.buttons}</span>
      </div>
    );
  }
}

/* Validate properties */
GridHeader.propTypes = {
  height: PropTypes.number.isRequired,
  rowCount : PropTypes.number.isRequired,
  totalRowCount: PropTypes.number.isRequired,
  buttons: PropTypes.element
};

/* Export the class and connect it to the redux store */
export default GridHeader;
