import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

/***
 * A canvas that is used to render content inside a viewport
 * Note: This is not a html-canvas.
 */
class ViewportCanvas extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.renderRows = this.renderRows.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.scroll = {scrollTop: 0, scrollLeft: 0};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.viewportWidth !== this.props.viewportWidth
      || nextProps.renderStartRowIndex !== this.props.renderStartRowIndex
      || nextProps.renderEndRowIndex !== this.props.renderEndRowIndex
      || nextProps.scrollToRowIndex !== this.props.scrollToRowIndex
      || nextProps.rowCount !== this.props.rowCount
      || nextProps.toggleRowUpdated !== this.props.toggleRowUpdated
      || nextProps.rowHeight !== this.props.rowHeight
      || nextProps.height !== this.props.height
      || nextProps.isScrolling !== this.props.isScrolling;
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    // Scroll to selected row
    let domNode =  ReactDOM.findDOMNode(this);
    if (!domNode) return;
    if (!this.props.isScrolling && this.props.scrollToRowIndex && (prevProps.scrollToRowIndex !== this.props.scrollToRowIndex)) {
      let shouldScroll = false;
      if (this.props.scrollToRowIndex > this.props.visibleEndRowIndex) {
        shouldScroll = true;
        this.scroll.scrollTop = this.scroll.scrollTop + (this.props.scrollToRowIndex-this.props.visibleEndRowIndex+1) * this.props.rowHeight;
      } else if (this.props.scrollToRowIndex < this.props.visibleStartRowIndex) {
        shouldScroll = true;
        this.scroll.scrollTop = this.scroll.scrollTop - (this.props.visibleStartRowIndex - this.props.scrollToRowIndex) * this.props.rowHeight;
      }
      if (this.props.onScroll && shouldScroll) {
        domNode.scrollTop = this.scroll.scrollTop;
        this.props.onScroll(this.scroll.scrollTop, this.scroll.scrollLeft);
      }
    }
  }

  onScroll(e) {
    if (ReactDOM.findDOMNode(this) !== e.target) return;
    let scrollLeft = e.target.scrollLeft;
    let scrollTop = e.target.scrollTop;
    if ((this.scroll.scrollLeft === scrollLeft) && Math.abs(scrollTop-this.scroll.scrollTop) < this.props.rowHeight) {
      return; // Workaround to avoid recursive loop when updating with inexact scroll position
    }
    // Notify parent of changed scroll position
    this.scroll = {scrollTop: scrollTop, scrollLeft: scrollLeft};
    if (this.props.onScroll) {
      this.props.onScroll(scrollTop, scrollLeft);
    }
  }

  // Render only the visible rows +/- overscan
  renderRows() {
    let rows = [];
    let rowHeight = this.props.rowHeight;
    let rowsToRender = this.props.renderEndRowIndex - this.props.renderStartRowIndex;
    let renderedRowsHeight = rowsToRender * rowHeight;
    let rowsToPreRender = this.props.visibleStartRowIndex - this.props.renderStartRowIndex;
    let preRenderedRowsHeight = rowsToPreRender * rowHeight;
    let preDivHeight = this.scroll.scrollTop - preRenderedRowsHeight;
    let totalScrollHeight = rowHeight * this.props.rowCount;
    let postDivHeight = totalScrollHeight - (preDivHeight + renderedRowsHeight);
    if (preDivHeight > 0) {
      rows.push(<div key="pre" className="grid-scroll-rows-before" style={{height: `${preDivHeight}px`}} />);
    }
    if (this.props.renderEndRowIndex > 0) {
      for (let i = this.props.renderStartRowIndex; i <= this.props.renderEndRowIndex; i++) {
        rows.push(this.props.rowRenderer(i));
      }
    }
    if (postDivHeight > 0) {
      rows.push(<div key="post" className="grid-scroll-rows-after" style={{height: `${postDivHeight}px`}} />);
    }
    //console.log(`renderRows: totalHeight: ${totalScrollHeight}, preRenderCount: ${rowsToPreRender}, preDivHeight: ${preDivHeight}, postDivHeight: ${postDivHeight}`);
    if (rows.length === 0) {
      rows.push(<div key="noData" style={{position: "absolute", textAlign: "center", top:`${this.props.height/2-10}px`, left: `${this.props.viewportWidth/2-20}px`}}>{this.props.placeholder}</div>);
    }
    return rows;
  }

  render() {
    return (
      <div className="viewport-canvas"  onScroll={this.onScroll} style={{position: "absolute", overflowX: "auto", overflowY: "scroll", top: "0px", left: "0px", width: `${this.props.viewportWidth}px`, height: `${this.props.height}px`}}>
        <div className="row-container">
          {this.renderRows()}
        </div>
      </div>
    );
  }
}

/* Validate properties */
ViewportCanvas.propTypes = {
  viewportWidth: PropTypes.number.isRequired,
  rowCount: PropTypes.number.isRequired,
  toggleRowUpdated: PropTypes.bool,
  visibleStartRowIndex: PropTypes.number.isRequired,
  visibleEndRowIndex: PropTypes.number.isRequired,
  renderStartRowIndex: PropTypes.number.isRequired,
  renderEndRowIndex: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  rowHeight: PropTypes.number.isRequired,
  rowRenderer: PropTypes.func.isRequired,
  onScroll: PropTypes.func,
  isScrolling: PropTypes.bool,
  scrollToRowIndex: PropTypes.number,
  placeholder: PropTypes.string.isRequired
};

/* Export the class and connect it to the redux store */
export default ViewportCanvas;
