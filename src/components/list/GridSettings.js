import React from 'react';
import PropTypes from 'prop-types';

class GridSettings extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onColumnVisibilityClicked = this.onColumnVisibilityClicked.bind(this);
    this.onSelectAllColumnsClicked = this.onSelectAllColumnsClicked.bind(this);
    this.onSaveColumnSettings = this.onSaveColumnSettings.bind(this);
    this.onCloseColumnSettings = this.onCloseColumnSettings.bind(this);
    this.state = {columns: this.props.columns, selectAllColumns: false};
  }

  onColumnVisibilityClicked(event) {
    let columnKey = event.currentTarget.getAttribute('data-field');
    let updatedColumns = this.state.columns.map(col => {
      return col.key === columnKey ? Object.assign({}, col, {isVisible: !col.isVisible}) : col;
    });
    this.setState({columns: updatedColumns});
  }

  onSelectAllColumnsClicked(event) {
    let selectAll = !this.state.selectAllColumns;
    let updatedColumns = this.state.columns.filter(c => c.key !== this.props.identityColumnKey).map(col => {
      return Object.assign({}, col, {isVisible: selectAll});
    });
    this.setState({selectAllColumns: selectAll, columns: updatedColumns});
  }

  onSaveColumnSettings(event) {
    this.props.onSaveSettings(this.state.columns);
  }

  onCloseColumnSettings(event) {
    this.props.onClose();
  }

  render() {
    let selectAllText = "Select All"; //TODO: Translate Select All
    let columns = this.state.columns.filter(c => c.key !== this.props.identityColumnKey); // Never show the identity column
    return (
      <div className="dropdown open" style={{position: "absolute", top:"0px", left: "0px", zIndex:"10000"}}>
        <div>
          <ul className="dropdown-menu dropdown-menu-justify" style={{minWidth: "200px", backgroundColor: "white", maxHeight: "300px", overflow: "auto", whiteSpace:"nowrap", right:"0"}}>
            <li key="all">
              <input style={{paddingTop: "10px"}} type="checkbox" data-field="all" value={selectAllText} checked={this.state.selectAllColumns} onChange={this.onSelectAllColumnsClicked}/>
              <span style={{fontSize: "0.75em", margin: "8px"}}>{selectAllText}</span>
            </li>
            {columns.map(column => (
              <li key={column.key}>
                <input style={{paddingTop: "10px"}} type="checkbox" data-field={column.key} value={column.header} disabled={column.isGroupBy} checked={column.isVisible} onChange={this.onColumnVisibilityClicked}/>
                <span style={{fontSize: "0.75em", margin: "8px"}}>{column.header}</span>
              </li>
            ))}
            <li className="grid-settings-buttons" key="save">
              <button type="button" onClick={this.onSaveColumnSettings}>Save settings</button>
              <button type="button" onClick={this.onCloseColumnSettings} style={{marginLeft: "10px"}}>Close</button>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

/* Validate properties */
GridSettings.propTypes = {
  identityColumnKey: PropTypes.string.isRequired,
  columns: PropTypes.array.isRequired,
  onSaveSettings: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

/* Export the class and connect it to the redux store */
export default GridSettings;
