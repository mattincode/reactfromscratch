import React from 'react';
import PropTypes from 'prop-types';
import {utils, write} from 'xlsx';
import FileSaver from 'file-saver';

class GridXlsxExport extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onExportClick = this.onExportClick.bind(this);
    this.getExportData = this.getExportData.bind(this);
    this.getBuffer = this.getBuffer.bind(this);
  }

  getBuffer(s) {
    if(typeof ArrayBuffer !== 'undefined') {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i=0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    } else {
      let buf = new Array(s.length);
      for (let i=0; i !== s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }
  }

  getExportData(rows, columns) {
    return rows.map(row => {
      let rowData = {};
      columns.forEach(col => {
        if (col.isVisible) {
          rowData[col.header] = row[col.key];
        }
      });
      if (!rowData) return;
      return rowData;
    });
  }

  onExportClick() {
    let exportData = this.getExportData(this.props.rows, this.props.columns);
    let workbook = utils.book_new();
    let worksheet = utils.json_to_sheet(exportData);
    utils.book_append_sheet(workbook, worksheet, this.props.workBookName);
    let wbOut = write(workbook, {bookType:'xlsx', bookSST:true, type: 'binary'});
    FileSaver.saveAs(new Blob([this.getBuffer(wbOut)],{type:"application/octet-stream"}), `${this.props.fileName}.xlsx`);
  }

  render() {
    return (
      <div>
        <span className="glyphicon glyphicon-export" onClick={this.onExportClick} />
      </div>
    );
  }
}

/* Validate properties */
GridXlsxExport.propTypes = {
  fileName: PropTypes.string.isRequired,      // Without file extension
  workBookName: PropTypes.string.isRequired,
  rows: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired
};

GridXlsxExport.defaultProps = {
  fileName: "GridExport",
  workBookName: "Export"
};

export default GridXlsxExport;
