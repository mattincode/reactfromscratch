import React from 'react';
import PropTypes from 'prop-types';

class GridSummaryRow extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.renderColumns = this.renderColumns.bind(this);
  }

  renderColumns() {
    let colPos = this.props.settingsColumnWidth+this.props.groupColumnsWidth-this.props.scrollLeft;
    let nextPos = colPos;
    let columns = [];
    // Add all visible columns
    this.props.columns.forEach(col => {
      if (!col.isVisible) return;
      let content = col.enableSummary ? this.props.onSummaryRowCallback(col.key, this.props.rows) : "";
      colPos = nextPos;
      nextPos = colPos + col.autoWidth;
      columns.push(
        <div key={col.key} className="grid-summary-column" style={{position: "absolute", top: "0px", left: `${colPos}px`, height:`${this.props.height}px` , width: `${col.autoWidth}px`,  textOverflow: "ellipsis", display: "inline-block", whiteSpace: "nowrap", transform: "none"}}>
          {content}
        </div>
      );
    });
    return columns;
  }

  render() {
    return (
      <div style={{position: "relative"}}>
        <div className="grid-summary" style={{position: "relative", height: `${this.props.height}px`}}>
          <div className="grid-summary-row" style={{position: "absolute", top: "0px", left: "0px", overflowX: "hidden", width: `${this.props.width}px`, minHeight: "auto"}}>
            <div style={{width: "100%", height: `${this.props.height}px`, overflow: "hidden", whiteSpace: "nowrap"}}>
              {this.renderColumns()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

/* Validate properties */
GridSummaryRow.propTypes = {
  identityColumnKey: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  columns: PropTypes.array.isRequired,
  rows: PropTypes.array.isRequired,
  scrollLeft: PropTypes.number.isRequired,
  settingsColumnWidth: PropTypes.number.isRequired,
  groupColumnsWidth: PropTypes.number,
  onSummaryRowCallback: PropTypes.func.isRequired
};

export default GridSummaryRow;
