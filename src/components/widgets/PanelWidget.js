
import React from 'react';
import PropTypes from 'prop-types';

const PanelWidget = ({id, title, children, isMaximized, onMaximize, onRestore}) => {

  let buttonIcon = isMaximized ?  <span className="icon-window-restore"/> : <span className="icon-window-maximize"/>;
  let buttonClickHandler = isMaximized ? function(event) {event.preventDefault(); onRestore(id);} : function(event) {event.preventDefault(); onMaximize(id);};
  let css = title !== undefined && title !== "" ? "panel-heading panel-heading-sm row" : "panel-heading panel-heading-sm row hide";
  return (
    <div className="panel-widget panel panel-default container-fluid">
      <div className={css}>
        <label className="panel-title pull-left" style={{marginLeft: "8px"}}>{title}</label>
        <button type="button" className="btn btn-xs pull-right" tabIndex="0" onClick={buttonClickHandler}>{buttonIcon}</button>
      </div>
      <div className="row">
        {children}
      </div>
    </div>


  );
};

PanelWidget.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
  children: PropTypes.element.isRequired,
  isMaximized: PropTypes.bool,
  onMaximize: PropTypes.func,
  onRestore: PropTypes.func
};

PanelWidget.defaultProps = {
  isMaximized : false
};

export default PanelWidget;
