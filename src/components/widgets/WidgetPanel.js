
import React from 'react';
import PropTypes from 'prop-types';

export const layoutTypes = {leftRightSplit: 2}; // Implement more types as needed!
export class WidgetOption {                     // Initial widget setup
  constructor(id, isMaximized) {
    this.id = id;
    this.isMaximized = isMaximized;
  }
}

class WidgetPanel extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {widgetOptions: this.props.widgetOptions};
    this.onMaximize = this.onMaximize.bind(this);
    this.onRestore = this.onRestore.bind(this);
    this.renderLeftRightSplit = this.renderLeftRightSplit.bind(this);
    this.onWindowResize = this.onWindowResize.bind(this);
  }

  componentDidMount() {
    window.addEventListener("resize", this.onWindowResize);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.widgetOptions) {
      let isUpdated = nextProps.widgetOptions.some(o =>  this.props.widgetOptions.find(wo => wo.id === o.id).isMaximized !== o.isMaximized);
      if (isUpdated) {
        this.setState({widgetOptions: nextProps.widgetOptions});
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.onWindowResize);
  }

  onWindowResize() {
    this.forceUpdate();
  }

  verifyWidgetLayout(widgets, layout) {
    let isLayoutOK = true;
    if (layout === layoutTypes.leftRightSplit && widgets.length !== 2) {
      isLayoutOK = false;
    }
    return isLayoutOK;
  }

  onMaximize(widgetId) {
    let widgetOptions = this.state.widgetOptions.map(option => {
      return Object.assign({}, option, {isMaximized: option.id === widgetId});
    });
    this.setState({widgetOptions: widgetOptions});
    if (this.props.onLayoutChange) {
      this.props.onLayoutChange(widgetId);
    }
  }

  onRestore(widgetId) {
    let widgetOptions = this.state.widgetOptions.map(option => {
      return Object.assign({}, option, {isMaximized: false});
    });
    this.setState({widgetOptions: widgetOptions});
    if (this.props.onLayoutChange) {
      this.props.onLayoutChange(0);
    }
  }

  renderLeftRightSplit(widgets, widgetsOptions) {
    let widgetId = 0;
    let Widgets = React.Children.map(widgets, widget => {
      let options = widgetsOptions[widgetId++];
      return React.cloneElement(widget, {id: options.id, isMaximized: options.isMaximized, onMaximize: this.onMaximize, onRestore: this.onRestore});
    });
    let maximizedWidgetOption = widgetsOptions.find(option => option.isMaximized);
    let maximizedWidgetId = maximizedWidgetOption ? maximizedWidgetOption.id : 0;
    return (
      <div style={{flex: "1", display: "flex", flexDirection: "row", height: "100%", width: "100%"}}>
        <div style={{flex: `${maximizedWidgetId === 0 ? 1 : 3}`, padding: "2px", minHeight: "100%"}}>{maximizedWidgetId < 2 ? Widgets[0] : Widgets[1]}</div>
        <div style={{flex: "1", padding: "2px", minHeight: "100%"}}>{maximizedWidgetId === 2 ? Widgets[0] : Widgets[1]}</div>
      </div>);
  }

  render() {
    let widgets = this.props.children;
    let options = this.state.widgetOptions;
    let layout = this.props.layoutType;
    if (!this.verifyWidgetLayout(widgets, layout)) {
      return (<div>The current layout: {layout} is not supported with {widgets.length} widgets</div>);
    }

    if (layout === layoutTypes.leftRightSplit) {
      return this.renderLeftRightSplit(widgets, options);
    }
  }
}

WidgetPanel.propTypes = {
  children: PropTypes.array.isRequired,
  layoutType: PropTypes.number.isRequired,
  widgetOptions: PropTypes.array,
  onLayoutChange: PropTypes.func,
};

WidgetPanel.defaultProps = {
  layoutType: layoutTypes.leftRightSplit,
  widgetOptions: [new WidgetOption(1, false), new WidgetOption(2,false)]
};

export default WidgetPanel;


