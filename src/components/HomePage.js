import React from 'react';
import PropTypes from 'prop-types';
//import List from './list/List';
import {GridColumn, SortOrder} from "./list/GridDefinitions";
import Icons from '../images/Icons';
import AutoComplete from './common/AutoComplete/AutoComplete';
import Grid from './list/Grid';
import {mockEmployees} from "../mockData/mockEmployees";
import Viewport from "./list/Viewport";
import WidgetPanel, {layoutTypes} from './widgets/WidgetPanel';
import PanelWidget from './widgets/PanelWidget';
import BaseComponent from './common/HOC/BaseComponent';
import ExtendedComponent from './common/HOC/ExtendedComponent';
import ErrorBoundry from './common/ErrorHandling/ErrorBoundry';
import ErrorBoundryTester from './common/ErrorHandling/ErrorBoundryTester';

const delay = 300;

class HomePage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.loadItemsAsync = this.loadItemsAsync.bind(this);
    this.onCustomFormatEmployeeNumber = this.onCustomFormatEmployeeNumber.bind(this);
    this.onScrollToRow = this.onScrollToRow.bind(this);
    this.onGridSummaryRow = this.onGridSummaryRow.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.items=[
      { number: 10020, label: 'apple long text test test', icon: Icons.Apple },
      { number: 10021, label: 'banana', icon: Icons.Banana },
      { number: 30010, label: 'arne', icon: Icons.Devil },
      { number: 30011, label: 'arne2', icon: Icons.Devil },
      { number: 30012, label: 'arne4', icon: Icons.Devil },
      { number: 30013, label: 'bamse', icon: Icons.Surprise },
    ];
    this.state = {
      scrollToKey: "",
      columns: [
        new GridColumn("employeeId", false, "Employee Id", null, SortOrder.None, false, null),
        new GridColumn("employeeNumber", true, "EmpNo", 60, SortOrder.Descending, false, null, this.onCustomFormatEmployeeNumber, false, null, true),
        new GridColumn("firstName", true, "Name", null, SortOrder.None, false, null, null, true),
        new GridColumn("gender", true, "Gender", 50, SortOrder.None, false, null, null, true, null, true, true),
        new GridColumn("postalCode", true, "PostCode", 50, SortOrder.None, false, null),
        new GridColumn("telephone1", true, "Phone 1", null, SortOrder.None, false, null),
        new GridColumn("telephone2", true, "Phone 2", null, SortOrder.None, false, null),
        new GridColumn("eMailAddress1", true, "mail", null, SortOrder.None, false, null),
        new GridColumn("city", true, "City", null, SortOrder.None, false, null),
      ],
      rows: mockEmployees
    };
  }

  onCustomFormatEmployeeNumber(row, value) {
    return <div>c {value}</div>;
  }

  loadItemsAsync(filter) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.items);
      }, delay);
    });
  }

  onScrollToRow() {
    if (this.state.scrollToKey === 2320) {
      this.setState({scrollToKey: 28778});
    } else {
      this.setState({scrollToKey: 2320});
    }
  }

  onGridSummaryRow(colKey, rowKeys) {
    return <div>{colKey}{rowKeys.length}</div>;
  }

  renderRow(index) {
    return <div key={index}>row {index}</div>;
  }

  render() {
    let buttons = <div>btn</div>;
    //let ExtendedComponent = withExtraStuff(BaseComponent, "extra");
    let selectedItems = this.items.filter(item => item.number < 30000);
    return (
    <div className="container">
      <h1 className="jumbotron">Home - Testpage</h1>
      <ErrorBoundry>
        <ErrorBoundryTester/>
      </ErrorBoundry>
      <h3>List example</h3>
      <WidgetPanel layoutType={layoutTypes.leftRightSplit}>
        <PanelWidget title="widget test">
          <div>
            <button type="button" onClick={this.onScrollToRow}>Scroll to key</button>
            <ExtendedComponent value="test" />
          </div>
        </PanelWidget>
        <PanelWidget title="List test">
          <Grid identityColumnKey="employeeId"
                columns={this.state.columns}
                rows={this.state.rows}
                selectedRowKey={this.state.scrollToKey}
                onSummaryRowCallback={this.onGridSummaryRow}
                buttons={buttons}
          />
        </PanelWidget>
      </WidgetPanel>
    </div>
    );
  }
}

export default HomePage;

/*
    <div className="container">
      <h1 className="jumbotron">Home - Testpage</h1>
      <h3>List example</h3>
      <List identityColumnKey="employeeId" columns={this.state.columns} rows={this.state.rows}/>
    </div>
    );
 */

/*
    <div className="container">
      <h1 className="jumbotron">Home - Testpage</h1>
      <h3>Autocomplete example:</h3>
      <AutoComplete items={this.items} />
      <h3>Autocomplete multiSelect example:</h3>
      <AutoComplete items={this.items} multiSelect selectedItemsMultiSelect={selectedItems} />
      <h3>Autocomplete async load example:</h3>
      <AutoComplete items={[]} loadItemsAsync onAsyncLoadItems={this.loadItemsAsync} />
      <div>Some random text...</div>
    </div>
 */
