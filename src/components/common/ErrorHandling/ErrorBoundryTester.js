import React from 'react';
import PropTypes from 'prop-types';

class ErrorBoundryTest extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onClick = this.onClick.bind(this);
    this.state = {triggerError: false};
  }

  onClick() {
    this.setState({triggerError: !this.state.triggerError});
  }

  render() {
    return (
      <div>
        <div style={{width:"500px", height: "200px", paddingTop: "40px", backgroundColor: "red", color: "white", fontSize:"24px"}} onClick={this.onClick}>Click me to get an error</div>
        {this.state.triggerError ?
          <span>{this.state.sss.sssss}</span>
          : null
        }
      </div>

    );
  }
}

/* Validate properties */
ErrorBoundryTest.propTypes = {
  //exampleTextProperty: PropTypes.string.isRequired,
};

/* Validate context (t is translation) */
ErrorBoundryTest.contextTypes = {
  t: PropTypes.func.isRequired
};


/* Export the class and connect it to the redux store */
export default ErrorBoundryTest;
