import React from 'react';
import PropTypes from 'prop-types';

export default class ErrorBoundry extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false };
  }

  componentDidCatch(error, info) {    
    this.setState({ error, info });
    if (this.props.onRenderError) {
      this.props.onRenderError(error);
    }
  }

  render() {
    if (this.state.error) {
      return (
        <div className="container-fluid">
          <h1>
            Error: {this.state.error.toString()}
          </h1>
          {this.state.info &&
          this.state.info.componentStack.split("\n").map(i => {
            return (
              <div key={i}>
                {i}
              </div>
            );
          })}
        </div>
      );
    }
    return this.props.children;
  }
}

ErrorBoundry.propTypes = {
  children: PropTypes.element.isRequired,
  onRenderError: PropTypes.func
};

