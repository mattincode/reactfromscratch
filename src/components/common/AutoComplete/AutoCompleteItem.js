import React from 'react';
import ReactDom from 'react-dom';
import PropTypes from 'prop-types';

class AutoCompleteItem extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {hover: false };
    this.onItemClick = this.onItemClick.bind(this);    
  }

  componentDidUpdate() {
    if (this.props.selected) {
      let listItem = ReactDom.findDOMNode(this);
      listItem.scrollIntoView(false);
    }
  }

  onItemClick(event) {    
    if (this.props.onItemClick) {
      this.props.onItemClick(this.props.number);
    }
  }

  render()
  {    
    let icon = this.props.icon ? <img src={this.props.icon} style={{ height: '24px',width: '24px' }}/> : "";
    let itemClass = this.props.selected ? "selected" : "";
    if (this.props.number < 0) {
      return <li style={{padding:"2px", color: "grey"}}>{this.props.label}</li>;
    }
    return (
      <li role="button" className={itemClass} style={{padding:"2px"}} onClick={this.onItemClick}>{icon}<span>{this.props.number} - {this.props.label}</span></li>
    );
  }  
}

AutoCompleteItem.propTypes = {
  icon: PropTypes.string,
  number : PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  selected: PropTypes.bool,  
  onItemClick: PropTypes.func
};

export default AutoCompleteItem;