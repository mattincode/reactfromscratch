/**
 * Autocomplete - Simple autocomplete control (only dependent on react)
 */
import React from 'react';
import PropTypes from 'prop-types';
import {keyCode} from "../../../models/constants/keyCodes";
import AutoCompleteItem from './AutoCompleteItem';

class AutoComplete extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.placeHolder = {number: -1, label: "No items found!" };
    this.placeHolderLoading = {number: -2, label: "Loading..." };
    this.userTypingDelayActive = false;
    this.onItemClick = this.onItemClick.bind(this);
    this.onChange = this.onChange.bind(this);
    this.filterItems = this.filterItems.bind(this);
    this.asyncFilterItems = this.asyncFilterItems.bind(this);
    this.onInputKeyDown = this.onInputKeyDown.bind(this);
    this.onInputFocus = this.onInputFocus.bind(this);    
    this.onRemoveMultiSelectItem = this.onRemoveMultiSelectItem.bind(this);
    this.onMultiSelectItemKeyDown = this.onMultiSelectItemKeyDown.bind(this);
    this.renderSingleSelect = this.renderSingleSelect.bind(this);
    this.renderMultiSelect = this.renderMultiSelect.bind(this);
    this.state = {inputText: this.props.inputText, dropdownItems: [], selectedItemNumber: null, selectedItems: this.props.selectedItemsMultiSelect};
  }

  onChange(e) {    
    if (!e || !e.target || !e.target.value) {
      this.setState({dropdownItems: [], inputText: ""});
      return;
    }
    let filter = e.target.value;    
    if (filter.length >= this.props.startFilterLength) {    
      if (this.props.loadItemsAsync && this.props.onAsyncLoadItems) {  
        this.asyncFilterItems(filter);
      } else {
        this.filterItems(filter, this.props.items);
      }
      this.setState({inputText: filter});        
    } else {
      this.setState({dropdownItems: [], inputText: filter});
    }    
  }

  onRemoveMultiSelectItem(itemToRemove) {
    this.setState({selectedItems: this.state.selectedItems.filter(item => item.number !== itemToRemove.number)});    
  }

  onMultiSelectItemKeyDown(event, item) {
    if (event.keyCode === keyCode.Delete) {
      this.onRemoveMultiSelectItem(item);
    }
  }

  //TODO: Add setTimeout to avoid several roundtrips
  asyncFilterItems(filter) {    
    if (this.userTypingDelayActive) return;
    this.userTypingDelayActive = true;
    setTimeout(() => {
      this.userTypingDelayActive = false;
      this.setState({dropdownItems: [this.placeHolderLoading]});
      this.props.onAsyncLoadItems(filter).then(items => {
        this.filterItems(filter, items);
      });      
    }, this.props.delayLoadAsync);
  }

  // filterItems will filter and sort according to user input (filter)
  filterItems(filter, itemsToFilter) {
    let items = itemsToFilter.filter(item => !this.state.selectedItems.some(selItem => selItem.number === item.number)); // Remove all already selected items from dropdown
    let regExp = new RegExp(`^[0-9]{${this.props.startFilterLength}}`); // Check if the starting text is numeric
    let startsWithNumber = filter ? (regExp.test(filter) ? true : false) : false;
    if (startsWithNumber) {
      items = items.filter(item => item.number.toString().startsWith(filter));
      items.sort();
    } else {
      items = items.filter(item => item.label.indexOf(filter) >= 0);
      items.sort(item => {return (item.label.localeCompare(filter) > 0) ? true : false;});
    }    
    if (items.length === 0) {
      items.push(this.placeHolder);
    }
    this.setState({dropdownItems: items, selectedItemNumber: items[0].number});
  }
  
  onInputFocus(event) {
    if (this.props.preSelectOnFocus) {
      event.target.select();
    }
  }

  onItemClick(selectedItemNumber) {
    if (selectedItemNumber <= 0) return;
    let selectedItem = this.state.dropdownItems.find(item => item.number === selectedItemNumber);
    if (this.props.multiSelect) {      
      let selectedItems = [...this.state.selectedItems, Object.assign({}, selectedItem)];
      this.setState({selectedItemNumber: "", selectedItems: selectedItems, dropdownItems: [], inputText: ""});
      if (this.props.onSelected) {
        this.props.onSelected(this.state.selectedItems);
      }
    } else {
      this.setState({selectedItemNumber: selectedItemNumber, dropdownItems: [], inputText: `${selectedItem.number} - ${selectedItem.label}`});
      if (this.props.onSelected) {
        this.props.onSelected(selectedItemNumber);
      }
    }
  }

  onInputKeyDown(event) {
    if (event.keyCode === keyCode.ArrowDown || event.keyCode === keyCode.ArrowUp) {
      event.preventDefault();
      event.stopPropagation();
      let selectedItemIndex = this.state.dropdownItems.findIndex(item => item.number === this.state.selectedItemNumber);
      if (event.keyCode === keyCode.ArrowDown && selectedItemIndex < (this.state.dropdownItems.length-1)) {
        this.setState({selectedItemNumber: this.state.dropdownItems[selectedItemIndex+1].number});
      } else if (event.keyCode === keyCode.ArrowUp && selectedItemIndex > 0) {
        this.setState({selectedItemNumber: this.state.dropdownItems[selectedItemIndex-1].number});
      }
    } else if (event.keyCode === keyCode.Enter || (this.props.tabAsEnter && !event.shiftKey && !event.ctrlKey && event.keyCode === keyCode.Tab)) {
      this.onItemClick(this.state.selectedItemNumber);
    } else if (event.keyCode === keyCode.Esc) {
      this.setState({dropdownItems: []}); // Esc will close the dropdown
    }
  }

  renderSingleSelect(dropdown) {
    return (
      <div>             
        <input className="dropdown-toggle" placeholder={this.props.placeholderText} onFocus={this.onInputFocus} onChange={this.onChange} value={this.state.inputText} onKeyDown={this.onInputKeyDown}/>
        {dropdown}
      </div>
    );
  }

  renderMultiSelect(dropdown) {    
    // Allow the use of arrow function for saving ref since this is recommended by the react-docs over using a string-ref.
    /* eslint-disable react/jsx-no-bind */
    let selectedItems = <div tabIndex="-1">{this.state.selectedItems.map(item => (<div className="token token-removable" tabIndex="0" key={item.number} onKeyDown={(event) => this.onMultiSelectItemKeyDown(event, item)}>{item.number} - {item.label}<span className="close-button" role="button" onClick={() => this.onRemoveMultiSelectItem(item)}>x</span></div>))}</div>; //TODO: Style!
    return (
      <div className="bootstrap-tokenizer clearfix form-control" style={{height:"auto", cursor: "text" }}>    
        {selectedItems}         
        <input className="dropdown-toggle bootstrap-tokenizer-input" style={{display:"inline-block", border:"none"}} onFocus={this.onInputFocus} onChange={this.onChange} value={this.state.inputText} onKeyDown={this.onInputKeyDown}/>
        {dropdown}
      </div>
    );
    /* eslint-enable react/jsx-no-bind */
  }

  render() {
    let dropdown = this.state.dropdownItems.length > 0 ?
    <div className="dropdown open" style={{zIndex:"10000"}}>
      <ul className="dropdown-menu dropdown-menu-justify">
        {this.state.dropdownItems.map(item => (<AutoCompleteItem key={item.number} number={item.number} label={item.label} icon={item.icon} selected={this.state.selectedItemNumber === item.number} onItemClick={this.onItemClick} />))}      
      </ul>
    </div>
    : "";
    
    if (this.props.multiSelect) {
      return this.renderMultiSelect(dropdown);
    } else {
      return this.renderSingleSelect(dropdown);
    }
  }
}

AutoComplete.propTypes = {    
  items: PropTypes.array,
  startFilterLength: PropTypes.number,
  placeholderText: PropTypes.string,
  inputText: PropTypes.string,  
  selectedItemsMultiSelect: PropTypes.array,
  onSelected: PropTypes.func,
  preSelectOnFocus: PropTypes.bool,
  tabAsEnter: PropTypes.bool,
  multiSelect: PropTypes.bool,
  loadItemsAsync: PropTypes.bool,
  onAsyncLoadItems: PropTypes.func,
  delayLoadAsync: PropTypes.number,
};

AutoComplete.defaultProps = {
  items: [],
  startFilterLength: 2,
  inputText: "",
  selectedItemsMultiSelect: [],
  placeholderText: "Enter value",
  preSelectOnFocus: true,
  tabAsEnter: true,
  multiSelect: false,
  loadItemsAsync: false,
  delayLoadAsync: 200
};

export default AutoComplete;
