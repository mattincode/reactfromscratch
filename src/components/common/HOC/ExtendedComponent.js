import React from 'react';
import PropTypes from 'prop-types';
import BaseComponent from "./BaseComponent";

export function withExtraStuff(Base, extraStuff) {
  return class WithStuff extends React.Component {
    constructor(props, context) {
      super(props, context);
      this.state = {extended: "ext"};
      this.onBaseChanged = this.onBaseChanged.bind(this);
    }

    onBaseChanged(val) {
      //console.log("Base changed: " + val);
    }

    render() {
      //console.log("Extended re-rendering");
      return (
        <Base
          {...this.props}
          onChange={this.onBaseChanged}
          extra = {extraStuff}
        />
      );
    }
  };
}

const ExtendedComponent = withExtraStuff(BaseComponent, "extra");

// Note that webstorm do notr
ExtendedComponent.PropTypes = {
  value: PropTypes.string.isRequired
};

export default ExtendedComponent;
