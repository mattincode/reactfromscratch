//https://hackernoon.com/higher-order-components-hocs-for-beginners-25cdcf1f1713

import React from 'react';
import PropTypes from 'prop-types';

class BaseComponent extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {internalValue: "hello"};
    this.onUpdateClick = this.onUpdateClick.bind(this);
  }

  onUpdateClick(event) {
    this.props.onChange(this.state.internalValue);
  }

  /* React lifecycle methods, order is important. see https://facebook.github.io/react/docs/react-component.html for more details */
  //componentWillMount() {}
  //componentDidMount() {}
  //componentWillReceiveProps(nextProps, nextContext) {}
  //shouldComponentUpdate(nextProps, nextState, nextContext) {}
  //componentWillUpdate(nextProps, nextState, nextContext) {}
  //componentDidUpdate(prevProps, prevState, prevContext) {}
  //componentWillUnmount() {}

  render() {
    //console.log("Base re-rendering");
    return (
      <div>
        <div>{this.props.value}</div>
        <div>{this.state.internalValue}</div>
        <div>{this.props.extra}</div>
        <button type="button" onClick={this.onUpdateClick}>Update parent</button>
      </div>
    );
  }
}

/* Validate properties */
BaseComponent.propTypes = {
  extra: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func
};

/* Export the class and connect it to the redux store */
export default BaseComponent;
