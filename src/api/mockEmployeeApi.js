const delay = 1000;
const EmployeeList =
  [
    {id: 11, name: "Nisse Hultman", phone: "044-122111"},
    {id: 12, name: "Karl Anka", phone: "046-112111"},
    {id: 13, name: "Martin Anka", phone: "044-122111"}
];

export function getEmployees(onSuccess) {
  return function(dispatch) {
    mockApiLoadEmployeeCall().then(employees => {
      dispatch(onSuccess(employees));  
    });
  };
}

function mockApiLoadEmployeeCall() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], EmployeeList));
      }, delay);
    });
}