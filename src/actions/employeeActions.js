import * as types from './actionTypes';
import {getEmployees} from '../api/mockEmployeeApi';

export function getAllEmployeesSuccess(employees) {
  return { type: types.GET_ALL_EMPLOYEES_SUCCESS, employees};
}

/**
 * Dispatches loading of the current user.
 * @returns {Function}
 */
export function loadEmployees() {
  return getEmployees(getAllEmployeesSuccess);  
}


