import DevilIcon from './Devil-03.png';
import SurpriseIcon from './Surprise.png';
import AppleIcon from './apple.png';
import BananaIcon from './banana.png';
import StrawberryIcon from './strawberry.png';

export default {
  Devil: DevilIcon,
  Surprise: SurpriseIcon,
  Apple: AppleIcon,
  Banana: BananaIcon,
  Strawberry: StrawberryIcon
};