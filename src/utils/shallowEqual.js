export function shallowEqual(objA, objB) {
  const hasOwnProperty = Object.prototype.hasOwnProperty;
  if (objA === objB) {
    return true;
  }
  if (typeof objA !== 'object' || objA === null ||
    typeof objB !== 'object' || objB === null) {
    return false;
  }
  let keysA = Object.keys(objA);
  let keysB = Object.keys(objB);
  if (keysA.length !== keysB.length) {
    return false;
  }
  // Test for A's keys different from B.
  let bHasOwnProperty = hasOwnProperty.bind(objB);
  for (let i = 0; i < keysA.length; i++) {
    if (!bHasOwnProperty(keysA[i]) || objA[keysA[i]] !== objB[keysA[i]]) {
      return false;
    }
  }
  return true;
}

export function isArrayEqual(arr1, arr2, key) {
  if (!arr1 && !arr2) return true;
  if (!arr1 && arr2) return false;
  if (arr1 && !arr2) return false;
  if (arr1.length !== arr2.length) return false;
  return arr1.every(a1 => shallowEqual(a1, arr2.find(a2 => a2[key] === a1[key])));
}

/**
 * isEqual: Checks if a simple value array [1,2..] or ["A","B",..] contain the same elements (made as function, not proptotype to avoid any name clashes)
 * @param array1
 * @param array2
 */
export function isValueArrayEqual(array1, array2) {
  if (!array1 && !array2) return true;
  if (!array1 && array2) return false;
  if (array1 && !array2) return false;
  if (!Array.isArray(array1) || !Array.isArray(array2)) return false;
  if (array1.length !== array2.length) return false;
  let length = array1.length;
  let isEqual = true;
  for (let i=0; i < length; i++) {
    if (array1[i] !== array2[i]) {
      isEqual = false;
      break;
    }
  }
  return isEqual;
}
