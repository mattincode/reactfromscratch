import webpack from 'webpack';
import path from 'path';
import CopyPlugin from 'copy-webpack-plugin';

module.exports = {
  devtool: 'source-map',
  entry: [
    'eventsource-polyfill',                       // necessary for hot reloading with IE
    'webpack-hot-middleware/client?reload=true',  //note that it reloads the page if hot module reloading fails.
    './src/index'
  ],  
  devServer: {
    contentBase: './src'
  },    
  target: 'web',
  output: {
    path: __dirname + '/dist',                  // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(),    
    new webpack.LoaderOptionsPlugin({
      noInfo: false,
      debug: true
    }),    
    new CopyPlugin([
      {from: 'src/index.html', to: 'index.html'},
      {from: 'node_modules/jquery/dist/jquery.min.js', to: 'jquery.js'}
    ])
  ],
  module: {
    rules: [
      {test: /\.js$/, include: path.join(__dirname, 'src'), use: [{loader: "babel-loader"}]},
      {test: /(\.css)$/, include: path.join(__dirname, 'src'), use: [{loader:'style-loader'}, {loader:'css-loader'}]},
      {test: /(\.css)$/, exclude: path.join(__dirname, 'src'),  use: [{loader:'style-loader'}, {loader:'css-loader'}]},
      {test: /\.(woff|woff2)$/, use: [{loader:"url-loader", options: {prefix: "font", limit: 5000}}]},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:'file-loader'}]},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:"url-loader", options: {limit: 10000, mimeType: "application/octet-stream"}}]},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:"url-loader", options: {limit: 10000, mimeType: "image/svg+xml"}}]},
      {test: /\.(jpg|png|gif)$/, include: path.join(__dirname, 'src/images'), use: [{loader: "url-loader", options: {limit: 25000}}]}
    ]
  }
};