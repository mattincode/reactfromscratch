# Barebone example using react, es2015 and webpack
Small example with minimal configuration.

## Prerequisites
1. **Install [Node 6](https://nodejs.org)**. Need to run multiple versions of Node? Use [nvm](https://github.com/creationix/nvm) or [nvm-windows](https://github.com/coreybutler/nvm-windows)
2. **Install babel-cli: `npm install -g babel-cli`


## Run
1. Open using Visual Studio Code
2. Run `npm install` in terminal (Show terminal: Ctrl+Ö)
3. Run `npm start` in terminal to build and start the express webserver and open in default browser

## Debug in VS Code (when running)
1. Install `Chrome Debug` extension (Ctrl+Shift+P, ext install + enter, -> 'Debugger for Chrome')
2. Press debug and use config `Launch Chrome against localhost` (defined in launch.json).

## Dependencies

### Production (Client side) Dependencies

| **Dependency** | **Use** |
|:----------|:-------|
|react|Main react codebase (Component, Proptypes...) |
|react-dom|React DOM-interaction, required to attach to the DOM in index.js |
|redux|Library for unidirectional data flows |
|react-redux|Redux library for connecting React components to Redux |
|redux-thunk|Async redux library|

### Development (Server side) Dependencies

    "babel-core": "^6.25.0",          // used by babel-loader to bundle inside webpack
    "babel-loader": "^7.1.1",         // used by webpack to bundle using babel
    "babel-preset-es2015": "^6.24.1", // used by babel-loader inside webpack to bundle ES2016 JS (config: .babelrc)
    "babel-preset-react": "^6.24.1",  // used by babel-loader inside webpack to bundle React JSX (config: .babelrc)
    "copy-webpack-plugin": "^4.0.1",  // used in webpack.config.js to copy index.html
    "express": "^4.15.4",             // use as dev webserver, will server index.html (see server.js)
    "path": "^0.12.7",                // used to handle path in node
    "webpack": "^3.4.1"               // Bundler that will create bundle.js in dist-folder (see webpack-config.js)
    "open": "0.0.5",    
    "redux-devtools-extension": "^2.13.2",
    "redux-immutable-state-invariant": "^2.0.0",    
    "webpack-dev-middleware": "^1.12.0",
    "webpack-hot-middleware": "^2.18.2"